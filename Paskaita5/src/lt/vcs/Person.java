/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author Vyte
 */
public class Person {
    private String name;
    private String surname;
    protected Gender gender;
    private int age;
    private String email;
    
    // reikia deklaruoti, kad konstruktorius ismeta Exception
    public Person(String name, String email) throws Exception {
        this.name = name == null ? name : name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        
        // c# if(email.IsNullOrEmpty())
        if (email ==null && email.trim().length() == 0 || email == "vardenis") {
            throw new Exception("Email must be not null.");
        } else{
        this.email = email;}
    }
    
    public Person(String name, String email, Gender gender) throws Exception {
         // iskviecia kita konstruktoriu, kad priskirtu varda
        this(name, email);
        this.gender = gender;
    }
    
    public Person(String name, String email, String surname, Gender gender, int age) throws Exception {
        this(name, email, gender);
        this.surname = surname;
        this.age = age;
    }

    @Override
    public String toString(){
       //return "Person (name: " + getName() + ", gender: " + gender.getEnLabel() + ").";
       return "" + getClass().getSimpleName() + " " + getName() + ", gender: " + gender.getEnLabel();
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    
}
