/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Vyte
 */
public class Main {

    public static void main(String[] args) {
      String line = inLine("Iveskite fraze.");
      
      line = line.replaceAll(" ", "");  // istrinami tarpeliai
      
      String[] lineMas = line.split(","); // iskaido i masyva, ten kur kablelis baigiasi, vienas string elementas, ir toliau iskaido zodzius. Kiti spaudos zenklai neitraukti
      List<String> lineList = createList(lineMas);      
      Set<String> lineSet = createSet(lineMas);
      
        out("------------ lineList-------------");
        outCollection(lineList);
        
       out("------------ lineSet-------------");
        outCollection(lineSet);
         out("------------ lineList-Sorted-------------");
        Collections.sort(lineList);
        outCollection(lineList);
       
        Map<String, Integer> map = new HashMap();
        
        map.put("obuolys", 5); // java primityvu int autoboxina i Integeri.
        
        // concurent ... exception
//        for (String s : lineList) { 
//            lineList.remove(s);
//        }
        
       Iterator<String> iter =  lineList.iterator();
        while (iter.hasNext()) { // tikrina ar toliau yra nariu, ar neismes exception.
            // zymeklis persikelia i sekancia reiksme, nes next metodas perzengia i kita, sekanti nari.
           String item = iter.next(); 
           // zymeklis keliauja nariu toliau kartojant cikla, kol baigiasi nariai. Nebus amzino ciklo.
           iter.remove();           
        }
         Integer val = map.get("obuolys");
         out(val);
         Set<String> keys = map.keySet(); // grazina map'o raktus, siuo atveju Stringus
         
         Collection<Integer> reiksmes = map.values(); // grazina map'o reiksmes-values, siuo atveju Integer'ius
         
//         for (Integer r : reiksmes){
//             // iteruoti ir kazka padaryti.
//         }


Player pl = null;
while (pl == null){
    

// isspausdina Exception teksta.
         try{
                pl = new Player(inWord("Iveskite varda"), inWord("Iveskite emaila"));
         } catch (Exception e){
        out(e.getMessage());
        }
}
         
    }   
    private static Set<String> createSet(String... strings){
        HashSet<String> result = new HashSet();
        if(strings != null){
          result.addAll(Arrays.asList(strings));
            } 
        return result;
        }
        
    private static List<String> createList(String... strings){
        List<String> result = new ArrayList();
        if (strings != null) {
            result.addAll(Arrays.asList(strings));
        } 
        return result;
    }
    
    private static void outCollection(Collection col){
        if (col != null) {
            col.forEach((item) -> {
                out(item);
            });
            
        }
    }
    
}
    

