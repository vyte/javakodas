package lt.vcs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import static lt.vcs.VcsUtils.*;

public class Main {

        public static void main(String[] args) {
        
            BufferedWriter bw = null;
            File pvzFile = new File(PVZ_FAILAS);
            BufferedReader br = null;
            String line = null;
            try {br = new BufferedReader(new InputStreamReader(new FileInputStream(pvzFile), UTF_8));
            // naujos reiksmes priskyrimas idetas i while cikla
            while  (br != null && (line = br.readLine()) != null){ 
                out(line);
            }            
            } catch (Exception e) {
                out(e.getMessage());
            } finally{
                if (br != null) {
                    
                    try {
                        br.close();
                    } catch(Exception e) {
                    out(e.getMessage());
                    }
                }           
            }
            
            File file = new File("C:\\temp\\failas.txt");
                      
                try {
                    bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), UTF_8));
              
            }  catch (Exception e) {
                out(e.getMessage());
            } finally{
                if (bw != null) {
                    
                    try {
                        bw.close();
                    } catch(Exception e) {
                    out(e.getMessage());
                    }

    }
                }
        }
        
        private static File newFile(String dir, String fileName){
            // patikrinti ar ne nullai
            // ar tai direktorija ir failo vardas, jei ne sukurti nauja direktorija ir failo varda
            // 
            File file = null;
            if (dir != null && fileName != null){                
              if (Files.isDirectory(Paths.get(dir))){
                  file = new File(dir, fileName);
              } else if(fileName != null){
                  try {
                      file = new File(Files.createDirectories(Paths.get(dir)).toString(), fileName);
                  } catch (IOException ex) {
                      out(ex.getMessage());
                  }                  
              } else{
                  out("Invalid input parameters: no file name.");
              }
            }
            // destytojo versija
//           File result = null;
//        if (dir == null || fileName == null) {
//            out("blogi parametrai");
//        } else {
//            File dira = new File(dir);
//            if (dira.isDirectory()) {  // isDicectory() taip pat tikrina, ar exists();
//                if (!dira.exists()) {
//                    try {
//                        dira.mkdirs();
//                    } catch (Exception e) {
//                        out(e.getMessage());
//                    }
//                }
//                result = new File(dira, fileName);
//                try {
//                    result.createNewFile();
//                } catch (Exception e) {
//                    out(e.getMessage());
//                }
//            } else {
//                out("ivedet bloga direktorija");
//            }
//        }
//        return result;
            
            return file;
        }
        
        private static final String PVZ_FAILAS = "C:\\temp\\pvz.txt";
        private static final String PVZ_DIR = "C:\\temp";
        private static final String UTF_8 = "UTF-8";
        
}
