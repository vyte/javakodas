package lt.vcs;

import static lt.vcs.VcsUtils.*;

public class Player extends Person{
    
    private int dice;
    
    
    public Player(String name, String email) throws Exception {
        // kviecia savo tevines klases konstruktoriu
       super(name, email);
       this.dice = random(1, 6);
       
    }
    
     public Player(String name, String email, Gender gender) throws Exception {
         // gender protected, tai leidzia ji cia priskirti
        super(name, email);
        this.gender = gender;
    }
     
     public Player(String name, String email, String surname, Gender gender, int age) throws Exception {
        this(name, email, gender);
        setSurname(surname);
        setAge(age);
    }

    public int getDice() {
        return dice;
    }

    
}
