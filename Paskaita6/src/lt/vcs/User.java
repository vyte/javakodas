/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author Vyte
 */
public class User<T extends Person> {
    // T - tai kazkoks dinaminis parametras
    private T person;
    
    public User(T person){
        this.person = person;
    }

    public T getPerson() {
        return person;
    }
    
}
