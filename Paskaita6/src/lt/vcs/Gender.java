package lt.vcs;

public enum Gender implements Named, Idable{
    
    MALE("Male", "Vyras", 1),
    FEMALE("Female", "Moteris", 0),
    OTHER("Other", "Kita", 2);
    
    private String enLabel;
    private String ltLabel;
    private int id;
    
    private Gender(String enLabel, String ltLabel, int id) {
        this.enLabel = enLabel;
        this.ltLabel = ltLabel;
        this.id = id;
    }
    @Override
      public int getId(){
          return id;
      }
    
    @Override
    public String getName(){
        return null;
    }
    
    public static Gender getById(int id) {
        for (Gender gen : Gender.values()) {
            if (id == gen.getId()) {
                return gen;
            }
        }
        return null;
    }
    
    public String getEnLabel() {
        return enLabel;
    }

    public String getLtLabel() {
        return ltLabel;
    }

    public int getId() {
        return id;
    }
    
}
