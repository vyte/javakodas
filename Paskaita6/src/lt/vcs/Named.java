package lt.vcs;


public interface Named extends Idable{
    public abstract String getName() throws BadDataInputException;
    
    public static Object newObj(){
        return new Object();
    }
    
}
