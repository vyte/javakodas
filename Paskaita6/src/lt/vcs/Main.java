/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static lt.vcs.VcsUtils.*;
/**
 *
 * @author Vyte
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Named g1 = Gender.FEMALE;
       Named g2 = null;
       
        try {
            g2 = new Person("abc", "bca");
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<Named> namedList = new ArrayList();
        namedList.add(g1);
        namedList.add(g2);
        
        for(Named n : namedList){
           try {
               out(n.getName());
               if (n instanceof Idable) {
                   Idable idable = (Idable) n;
               }
           } catch (BadDataInputException ex) {
               Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
           }
        }
        
        
        
    }
    
}
