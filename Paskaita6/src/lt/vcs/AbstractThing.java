package lt.vcs;
import static lt.vcs.VcsUtils.*;

public abstract class AbstractThing implements Idable{
    
    // abstrakti klase, paveldejo abstraktu metoda, jo nereik cia igyvendinti
    // reikia rasyti modifikatorius. Negali buti private; protected, public arba default
    
    // jei turi nors viena abstraktu metoda, klase yra abstrakti.
    public abstract Object naujasObjektas();
    
    public void bendrasFunkcionalumas(){
        out("" + naujasObjektas() + getId());
    }
    
    
}
