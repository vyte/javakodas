/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class Main {

  
    public static void main(String[] args) {
       bodyWeightIndex();
    }
    
//    1a. Parašyti programą, kuri paprašytų įvesti 5 skaičius. Baigus skaičių įvedimą, turi būti
//atvaizduojama įvestų skaičių suma ir visi įvesti skaičiai.

    public static void sumOfFive(){
            int skaicius;
            int suma = 0;
            
            ArrayList<Integer> sk = new ArrayList<>();        
            for (int i = 0; i < 5; i++) {
              skaicius = inNumber("Ivedinekite  5 skaicius, tada isspausdinsim suma.");
            sk.add(skaicius);
            suma = suma + skaicius; 
        }       
            for (int i = 0; i < 4 ; i++) {
            System.out.print(String.valueOf(sk.get(i)) + " + ");
        }
            System.out.print(String.valueOf(sk.get(4)));
            System.out.print(" = " + String.valueOf(suma));           
        }
    
//1b. Parašyti programą kuri paprašytų vartotojo įvesti 5 žodžius. Po to kai bus įvesti visi
//žodžiai, jie turi būti atvaizduojami.
    
    public static void printFiveWords(){
        ArrayList<String> words = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            String w = inWord("Iveskite " +(i + 1) + " zodi:");
            words.add(w);
        }
        words.forEach((item) -> {
            System.out.println(item);
        });
    }

//2a. Parašyti programą kuri leistų vartotojui įvesti savo masę ir ūgį, pagal gautus duomenis
//paskaičiuotų ir atvaizduotų jo kūno masės indeksą. KMI = masė (kg) / (ūgis(m))^2

    public static void bodyWeightIndex(){
        double weight = inDouble("Iveskite savo svori kg:");
        double height = inDouble("Iveskite ugi m:");
        
        double index = weight/Math.pow(height,2);
        out("Kuno mases indeksas: " + index);
    }
 
//2b. Paprašyti vartotojo nurodyti kiek skaičių jis ketina įvesti, tuomet duoti jam įvesti tiek
//skaičių, kiek jis pasirinko. Po įvedimo atvaizduoti tik tuos skaičius kurie buvo didesni nei 100.
    
    public static void moreThanHundred(){
        int n = inNumber("Kiek skaiciu norite ivesti?");
        int[] numbers = new int[n];
        for (int i = 0; i < n; i++) {
            int input = inNumber("Iveskite " + (i+1) + " skaiciu:");
            numbers[i] = input;            
        }
         ArrayList<Integer> result = new ArrayList<>(); 
      for (int item : numbers) {
    System.out.println(item);
    //from(numbers).where("age", lessThan(20)).all();
}
        
        //out(result);
    }
    
    
    public static int inNumber(String txt){
           out(txt);
            return newScan().nextInt();
       }
    
    public static double inDouble(String txt){
        out(txt);
        return newScan().nextDouble();
    }
       
       public static String inWord(String txt){
           out(txt);
           return newScan().next();
       }
         private static void out(String txt){
        System.out.println(txt);
    }
         
         private static Scanner newScan(){       
           return new Scanner(System.in);
       }
}
