/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.ArrayList;
import java.util.Scanner;


public class Savaitgalis1nd {


    public static void main(String[] args) {
        showNumber();
     
        
        
    }
    
    private static void out(String txt){
        System.out.println(txt);
    }
    
//    2) a) paprašyti naudotojo įvesti skaičių nuo 1 iki 10, jei įvestas skaičius didesnis už 5 išvesti į konsolę "virs 5", jei mažesnis išvesti "maziau 5", jei mažiau už 1 arba daugiau už 10 išvesti "ivestas blogas skaicius";
//b*) 2a užduoties metodą padaryti rekursiniu(rekursinis metodas tai metodas, kuris savo viduje iškviečia pats save, saugokites amžino pasikartojimo), t.y. jeigu įvestas sk maziau už 1 ar daugiau už 10 išvesti tą patį blogo sk tekstą ir iš naujo prašyti įvesti skaičių nuo 1 iki 10 kol nebus patenkinta sąlyga;
    
// neveikia, netikrina ar daugiau ar maziau
       public static void showNumber(){
int skaicius = ivestiSkaiciu("Iveskite skaiciu nuo 1 iki 10:");
boolean baigti;
        baigti = (skaicius < 1 && skaicius  > 10);
while (!baigti){
   out("Ivestas blogas skaicius. Bandykite dar karta.");
   skaicius = ivestiSkaiciu("Iveskite skaiciu nuo 1 iki 10:");
   baigti = (skaicius < 1 && skaicius  > 10);
}
if (skaicius > 5) {
   out("Virs 5");
}else{
    out("Maziau 5.");
}    
}
    
//       3) sveikų skaičių kalkuliatorius:
//naudotojo paprašoma įvesti pirmą skaičių;
//naudotojui leidžiama pasirinkti, kokį veiksmą jis nori atlikti:
//1 - sudėtis
//2 - atimtis
//3 - daugyba
//4 - dalyba
//naudotojo paprašoma įvesti antrą operacijos skaičių veiksmui atlikti.
//Gale į konsolę išvesti matematinę atitikmenį ir rezultatą, pvz:
//naudotojas ivedė 5, pasirinko 3(daugyba), įvedė 7, rezultate, konsolėje turime pamatyti tekstą: "5 * 7 = 35";
//P.S. jei dalyba ir antras skaičius 0 neleisti atlikti operacijos, nes dalyba iš 0 negalima :P


       public static void calculator(){
      
int pirmasSkaicius = ivestiSkaiciu("Iveskite pirma skaiciu:");
int veiksmas = ivestiSkaiciu("Koki veiksma norite atlikti? \n1 - sudėtis\n" +
"2 - atimtis\n" +
"3 - daugyba\n" +
"4 - dalyba");
           
int antrasSkaicius = ivestiSkaiciu("Iveskite antra skaiciu:");
int skaicius;
switch (veiksmas){
    case 1:
        skaicius = pirmasSkaicius + antrasSkaicius;
        out("Sudetis: " + pirmasSkaicius  + " + " + antrasSkaicius + " = "  + skaicius);
        break;
        case 2:
        skaicius = pirmasSkaicius - antrasSkaicius;
        out("Atimtis: " + pirmasSkaicius  + " - " + antrasSkaicius + " = "  + skaicius);
         break;
        case 3:
        skaicius = pirmasSkaicius * antrasSkaicius;
        out("Daugyba: " + pirmasSkaicius  + " * " + antrasSkaicius + " = "  + skaicius);
         break;
        case 4:
            if (antrasSkaicius != 0) {
                skaicius = pirmasSkaicius / antrasSkaicius;
        out("Sudetis: " + pirmasSkaicius  + " / " + antrasSkaicius + " = "  + skaicius);
            } else{
                out("Dalyba is 0 negalima.");
            }
             break;
}
       }
        //4) a) Paprašyti naudotojo įvesti žodį, ir į konsolę išvesti skaičių, kiek simbolių įvedė naudotojas tame žodyje.
//P.S. ieškokite metodo String klasėje, skaitykite dokumentaciją.
//b*) papildomai taip pat išspausdinti įvestą žodį didžiosiomis raidėmis, vėl gi String klasėje ieškoti..
//P.S. atkreipkite dėmesį kaip deklaruoti metodai, ar jie grąžina rezultatą, ar redaguoja esamą objektą...
        
        public static void kiekSimboliuZodyje(){
            out("Iveskite zodi:");
            Scanner sc  = new Scanner(System.in);
            String zodis = sc.next();            
            out("Zodis turi " + String.valueOf(zodis.length()) + " raidziu.");         
            out(zodis.toUpperCase());
        }
        
//        5) a) naudotojo prašyti įvedinėti sveikus skaičius tol, kol įves 0 ir į konsolę išvesti įvestų sk. sumą;
//b*) matematiškai atvaizduoti įvestus skaičius ir rezultatą išvedime, pvz.: 1 + 3 + 5 + 7 + 9 + 0 = 25


        public static void skaiciuSuma(){
            int skaicius;
            int suma = 0;
           do {
            skaicius = ivestiSkaiciu("Ivedinekite skaicius, kai ivesite 0, isspausdinsim suma.");
            suma = suma + skaicius;            
        } while (skaicius != 0);
           out(String.valueOf(suma));
        }
        
        public static void skaiciuSumaIspausdinta(){
            int skaicius;
            int suma = 0;
            
            ArrayList<Integer> sk = new ArrayList<>();        
           do {
            skaicius = ivestiSkaiciu("Ivedinekite skaicius, kai ivesite 0, isspausdinsim suma.");
            sk.add(skaicius);
            suma = suma + skaicius;            
        } while (skaicius != 0);
           sk.forEach((item) -> {
               if (item != 0) {
                   System.out.print(String.valueOf(item) + " + ");
               } else{
                   System.out.print(String.valueOf(item));
               }
        });
           System.out.print(" = " + String.valueOf(suma));
        }
        
        //6) paprašyti vartotojo įvesti žodį ir jį iš karto atvaizduotų, tada vėl paprašytų įvesti kitą žodį, jį atvaizduotų, ir procesą kartotų tol, kol nebus įvestas žodis "stop".

       public static void spausdintiZodzius() {
           String zodis = ""; 
           
           while  (!zodis.equalsIgnoreCase("stop")){
               zodis = ivestiZodi("Iveskite zodi, kuri atspausdinsime. Rasykite 'stop', jei norite sustoti.");
                out(zodis);
           }          
       }
       
       //7) a) į konsolę atspausdinti daugybos lentelę skaičiam nuo 1 iki 9, pvz.:
//1 * 1 = 1         2 * 1 = 2 
//1 * 2 = 2         2 * 2 = 4
//.......                ..........
//1 * 10 = 10     2 * 10 = 20
//ir t.t
       
       public static void atspausdintiDaugybosLentele(){
           for (int i = 1; i <= 10; i++) {
               for (int j = 1; j <= 10; j++) {
                   int result = i * j;
                   System.out.println(i + " * " + j + " = " + result );
               }
               out(" ");
           }
       }
       
       //b*) paprašyti naudotojo ivesti iki kokio skaičiaus daugybos lentelę jis nori atsispausdinti į konsolę, pradedam nuo 1 iki įvesto sk.
       
       public static void spausdintiDaugIkiSkaiciaus(){
           int sk = ivestiSkaiciu("Iveskite skaiciu, iki kurio spausdinsime daugybos lentele");
                   for (int i = 1; i <= sk; i++) {
               for (int j = 1; j <= 10; j++) {
                   int result = i * j;
                   System.out.println(i + " * " + j + " = " + result );
               }
               out(" ");
           }
           
       }
       
       public static int ivestiSkaiciu(String txt){
           out(txt);
           Scanner sc = new Scanner(System.in);
            int skaicius = sc.nextInt();
            return skaicius;
       }
       
       public static String ivestiZodi(String txt){
           out(txt);
            Scanner sc  = new Scanner(System.in);
            String zodis = sc.next();
            return zodis;
       }
}
