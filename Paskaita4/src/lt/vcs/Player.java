/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import static lt.vcs.VcsUtils.*;

public class Player extends Person{
    
    private int dice;
    
    public Player(String name) {
        // kviecia savo tevines klases konstruktoriu
       super(name);
       this.dice = random(1, 6);
    }
    
     public Player(String name, Gender gender){
         // gender protected, tai leidzia ji cia priskirti
        this(name);
        this.gender = gender;
    }
     
     public Player(String name, String surname, Gender gender, int age){
        this(name, gender);
        setSurname(surname);
        setAge(age);
    }

    
    


    public int getDice() {
        return dice;
    }


    
}
