/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static lt.vcs.VcsUtils.*;

public class Main {

    public static void main(String[] args) {

    Player p1 = new Player(inWord("Iveskite varda: "), Gender.getById(inInt("Iveskite lyti: 0 - moteris, 1 - vyras, 2 - kita.")));
Person per1 = p1;
Object obj = p1;
out(p1);
out(per1);
out(obj);

// extendinus T su Person, nebeleidzia deklaruoti su String
    User<Person> u1 = new User<Person>(new Person("Luciferis"));  
    
    User<Player> u2 = new User(p1); // u2.getPerson(). rodo Player metodus
    out(u1.getPerson());
    out(u2.getPerson());
    
    User[] uMas = {u1, u2}; // beparametris masyvas, jei kvieciam uMas[0].getPerson(). rodo tik Object metodus
    
    List list = new ArrayList();
    list.add(u1);
    list.add(u2);
    
    List<String> strList = new ArrayList();
    Set<String> strSet = new HashSet();
    
    strList.add("bla");
    strList.add("bla");
    strList.add("bla");  
            
    
}

}
