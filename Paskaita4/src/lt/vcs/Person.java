/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author Vyte
 */
public class Person {
    private String name;
    private String surname;
    protected Gender gender;
    private int age;
    
    public Person(String name){
        this.name = name == null ? name : name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
    }
    
    public Person(String name, Gender gender){
         // iskviecia kita konstruktoriu, kad priskirtu varda
        this(name);
        this.gender = gender;
    }
    
    public Person(String name, String surname, Gender gender, int age){
        this(name, gender);
        this.surname = surname;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
}
