package lt.vcs;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import static lt.vcs.VcsUtils.*;

public class Main {
    
public  static final String SELECT_LAST_ID_SQL =  "select MAX (id) from ?;";

    public static void main(String[] args) {       

        String url = "jdbc:mysql://localhost:3306/";
        // String driver = "com.mysql.jdbc.Driver";
        String dbName = "vcs_17_04";
        String userName = "root";
        String password = "";
        Connection conn = null;
        
        
        try{
        conn = DriverManager.getConnection(url + dbName, userName, password);
        // result set galim skrolinti, ir jautrus atnaujinimams.
        Statement s = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        
        ResultSet rs = s.executeQuery("select * from car");
        out("Isspausdinsim visas reiksmes is car lenteles pries istrynima:");
        outCarResultSet(rs); 
//         rs.beforeFirst();
//            for (int i = 1; rs.next(); i++) {
//                rs.updateString("make", rs.getString("make") + " (kazka kompensuoja) " + i );
//                rs.updateRow();
//            }            
         out("Isspausdinsim visas reiksmes is car lenteles po istrynimo:");      
            rs.moveToInsertRow();
            rs.updateInt(1, 4);
            rs.updateString("make", "Zapukas");
            rs.updateString("name", "sens, bet gers ir raudons");
            rs.insertRow();
//                rs.afterLast();
//                rs.previous();
//                rs.deleteRow();

            outCarResultSet(rs);
            
         
            
          
        
    } catch (Exception e) {
        out("Nepavyko prisijungti prieDB " + e.getMessage());
        throw new RuntimeException(e);
    } finally {
            if  (conn != null){
                try {
                    conn.close();
                } catch (Exception e){
                    out(e.getMessage());
                }
            }
        }
    }
         public static void outCarResultSet(ResultSet rs){
        try {
        rs.beforeFirst();        
            while(rs.next()){
                out("id = " + rs.getInt(1) + ", make = '" + rs.getString(2) + "', name = '"+ rs.getString(3) + "'.");
            } 
        } catch (SQLException ex) {
            out(ex.getMessage());
        }
        
    }
}
 