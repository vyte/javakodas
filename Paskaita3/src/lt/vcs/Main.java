package lt.vcs;
//statiskai importuoja visus tos klases narius
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.Gender.getById;
import java.util.stream.*;



public class Main {

    public static void main(String[] args) {
//        String s = inWord("Iveskite 3 raidziu zodi: ");
//        String z = inWord("Iveskite zodi: ");
//        if (s.equals("abc")) {
//            out("tikrai abc");
//        } else {
//            out("ne abc");
//        }
//        // jei nezinoma ar objektas ne null
//        if ("abc".equals(s)) {
//            out("tikrai abc");
//        } else {
//            out("ne abc");
//        }
//        // dorai neveikia
////        String result = null;
////        String result = (s.equals("abc")) ? "tikrai abc" : "ne abc";
//
//out(new Date());
//
//SimpleDateFormat sdf = new SimpleDateFormat("'Data: ' yyyy-MM-dd 'Laikas: ' HH:mm:ss");
//out(sdf.format(new Date()));
//
//out(random(1, 10));
//out(Gender.FEMALE.getLtLabel());

ArrayList<Player> players =  new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            String n = inWord("Iveskite " +(i+1)+ " zaidejo varda.");
            Gender g = getById(inNumber("Iveskite " +(i+1)+ " zaidejo lyti: 0-moteris, 1-vyras, 2-kita."));
            Player p = new Player(n, g);
            players.add(p);            
        }
        int game = inNumber("Pasirinkite zaidima: 1 - vienas metimas, 2 - penkiu metimu suma.");
        switch (game){
            case 1:
                 if (players.get(0).getDice() > players.get(1).getDice()) {
            out("" + players.get(0).getName() + " ismete: " + players.get(0).getDice() + ". " + players.get(1).getName() + " - "+ players.get(1).getDice() + "." + " Pirmasis laimejo.");
        } else if (players.get(0).getDice() < players.get(1).getDice()) {
            out("" + players.get(0).getName() + " ismete: " + players.get(0).getDice() + ". " + players.get(1).getName() + " - "+ players.get(1).getDice() + "." + " Antrasis laimejo.");
        } else {
            out("Abu ismete: " + players.get(0).getDice() + " Taigi lygiosios.");
        }
                 break;
            case 2:
                int sum1 = IntStream.of(players.get(0).getDiceHand()).sum();
                int sum2 = IntStream.of(players.get(1).getDiceHand()).sum();
                if (sum1 > sum2) {
                    out("" + players.get(0).getName() + " laimejo. Jo kauliuku suma yra: " +  sum1 + ".");
                } else if (sum1 < sum2) {
                    out("" + players.get(1).getName() + " laimejo. Jo kauliuku suma yra: " +  sum1 + "." );
                } else {
                    out("Lygiosios.");
                }
                break;
            default:
                out("Tokio zaidimo nezaidziame.");
                break;                
        }
       

    }
    
}
