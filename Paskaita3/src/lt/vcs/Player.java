/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Vyte
 */
public class Player {
    
    private String name;
    private int dice;
    private Gender gender;
    private int[] diceHand;

    public Player(String name, Gender gender){
        
        //this.name = Character.toUpperCase(name.charAt(0)) + name.substring(1);
        
        //substring(pradzios index, end index), antro indekso i substringa neitraukia. Grazina nauja stringa.
        // tikrina ar string ne null, ir tik tada kviecia metodus. Jei name == null, tai player.name = null.
        this.name = name == null ? name : name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
        this.dice = random(1, 6);
        this.gender = gender;
        this.diceHand = new int[5];
        for (int i = 0; i < 5; i++) {
            this.diceHand[i] = random(1, 6);            
        }
    }
    
    
     public int getDice() {
        return dice;
    }
    public void setDice(int dice) {
        this.dice = dice;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName(String n){
       this. name = n;
    }
      public int[] getDiceHand() {
        return diceHand;
    }
}
