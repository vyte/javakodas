package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class VcsUtils {
    
    public static int random(int from, int to){
        return ThreadLocalRandom.current().nextInt(from, to + 1);
    }
    
    public static String inLine(String txt){
        out(txt);
        return newScan().nextLine();
    }
    
        private static String timeNow(){
        SimpleDateFormat sdf = new SimpleDateFormat("'Laikas: {' HH:mm:ss:SSSS'}'");
        String s = sdf.format(new Date());
        return s;
    }
    
            public static int inNumber(String txt){
           out(txt);
            return newScan().nextInt();
       }
       
        public static String inWord(){
            return newScan().next();
        }
        
       public static String inWord(String txt){
           out(txt);
           return newScan().next();
       }
         public static void out(Object txt){
        System.out.println(timeNow() + " " + txt);
    }
         
         public static Scanner newScan(){       
           return new Scanner(System.in);
       }    
}
