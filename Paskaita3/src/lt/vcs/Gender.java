/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

public enum Gender {
    MALE(1, "Male", "Vyras"), 
    FEMALE(0, "Female", "Moteris"),
    OTHER(2, "Other", "Kita");
    
    private int id;
    private String enLabel;
    private String ltLabel;
    
    private Gender(int id, String enLabel, String ltLabel){
        this.id = id;
        this.enLabel = enLabel;
        this.ltLabel = ltLabel;
    }

    public static Gender getById(int id) {
        
        for (Gender gen : Gender.values()) {
            if (id == gen.getId()) {
                return gen;
            }
        }
        return Gender.OTHER;
    }
    
//    public static Gender getById(int id){
//        
// for (Gender g : Gender.values()) {
//      if (g.getId() == id) {          
//        return g;
//      }
//   } 
//     return null;
//    }
    
    public String getEnLabel() {
        return enLabel;
    }

    public void setEnLabel(String enLabel) {
        this.enLabel = enLabel;
    }

    public String getLtLabel() {
        return ltLabel;
    }

    public void setLtLabel(String ltLabel) {
        this.ltLabel = ltLabel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
