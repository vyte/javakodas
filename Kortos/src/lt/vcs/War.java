package lt.vcs;

import java.util.ArrayList;
import java.util.List;
import static lt.vcs.CardUtils.*;
import static lt.vcs.VcsUtils.*;

public class War {
   //metodas kur prasidės kortu zaidimas "karas". Žaidimas skirtas 1 žaidėjui, žaisti prieš kompiuterį.
    static List<Card> playerHand  = new ArrayList();
    static List<Card> computerHand  = new ArrayList();
    
   static List<Card>  deck  = createDeck();
    
    public static void distributeCards(){
      shuffleCards(deck);
      
      //zaidejo kortos
        for (int i = 0; i < 26; i++) {
            int random = random(0, 51);
            Card e = deck.get(random);            
            playerHand.add(e);  
            deck.remove(random);            
        }
        // kompiuterio kortos
         for (int i = 0; i < 26; i++) {
            int random = random(0, 51);
            Card e = deck.get(random);            
            computerHand.add(e);  
            deck.remove(random);            
        }
    }   
        public static void round(){
                    
    }
        public static Card getCard(List<Card> hand){
            int i = random(0, hand.size());
            Card card = hand.get(i);
            hand.remove(i);            
            return card;   
        }
    
public void start() {
    out("Prasideda zaidimas! Isdalintos kortos!");
    distributeCards();
    
    
}


//Žaidėjui duodamas tik vienas veiksmas dėti kortą, čia kad automatizuotai per pora sekundžių nesusižaistų automatiškai partija :) Priklausomai nuo to ar korta kurią padeda žaidėjas ar jo priešininkas atversta ar užversta informuoti apie tai kokia ten korta ir kaip vyksta žaidimo eiga.

    
}
