package lt.vcs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardUtils {
    //metodas skirtas sukonstruoti kortu kaladę su kortomis nuo 2 iki tūzo(A), zinoma visu zenklu po viena, viso 52 kortos.
    public static List<Card> createDeck(){
        List<Card> deck = new ArrayList();
        
        for (int i = 2; i <= 10; i++) {
            for (Suit s : Suit.values()) {
                Card card = new Card(s, "" + i);
                deck.add(card);
            }            
        }
        
           for (Suit s : Suit.values()) {
                Card card = new Card(s, "Ace");
                deck.add(card);
            } 
           
               for (Suit s : Suit.values()) {
                Card card = new Card(s, "King");
                deck.add(card);
            }      
               
                for (Suit s : Suit.values()) {
                Card card = new Card(s, "Queen");
                deck.add(card);
            } 
                
                    for (Suit s : Suit.values()) {
                Card card = new Card(s, "Jack");
                deck.add(card);
            } 
        return deck;
    } 
    
    //metodas ismaisyti kortas
public static void shuffleCards(List<Card> cards) {
    if (cards != null) {
        Collections.shuffle(cards);
    }
}

    
}
