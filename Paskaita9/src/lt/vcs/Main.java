package lt.vcs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import static lt.vcs.VcsUtils.*;

public class Main {


    public static void main(String[] args) throws SQLException {
        
                String url = "jdbc:mysql://localhost:3306/";
        // String driver = "com.mysql.jdbc.Driver";
        String dbName = "vcs_17_04";
        String userName = "root";
        String password = "";
        Connection conn = null;
        
        try{
        conn = DriverManager.getConnection(url + dbName, userName, password);
        out("Valio prisijungem prie DB!!!! " + conn.getSchema());
        Statement s = conn.createStatement();
        ResultSet result = s.executeQuery("select * from person;");
        out(result);        
        while(result.next()){
           String email = result.getString("email");
           String name = result.getString(2);
           int gender = result.getInt("gender");
           out("" + name +" " + gender + " " + email);
        }        
        out(result.getMetaData().getColumnType(2)); // parodo duomenu tipa: 12 - varchar
        
        s = conn.createStatement();
        s.executeUpdate("insert into person values(" + (getLastId(s, "id") +1) + ", 'As', 'Mano', 2, 77, 'as@mano.lt' ");
        
    } catch (Exception e) {
        out("Nepavyko prisijungti prieDB " + e.getMessage());
    } finally {
            if  (conn != null){
                try {
                    conn.close();
                } catch (Exception e){
                    out(e.getMessage());
                }
            }
        }
        
        
    
}
    
    private static Integer getLastId(Statement s, String table){
        Integer result = null;
        // ("select MAX (id) from " + table;)
        // result = rs.getInt(1);
        
        try { 
            ResultSet rs = s.executeQuery("SELECT id FROM " + table + " ORDER BY id DESC limit 1");    
            if (rs.next()){
                result = rs.getInt("id");
            }
            
        } catch (SQLException ex) {
            out(ex.getMessage());
            
//        } finally{
//            if  (conn != null){
//                try {
//                    conn.close();
//                } catch (Exception e){
//                    out(e.getMessage());
//                }
//            }
        }
        
        return result;
    }
}
